package com.mycompany.facturation;

/**
 *
 * @author nirai
 */
public class ClientParticulier extends Client{
    private String civilte;
    private String nom;
    private String prenom;

    public ClientParticulier(String civilte, String nom, String prenom, String refClient) {
        super(refClient);
        this.civilte = civilte;
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getCivilte() {
        return civilte;
    }
    public String getNom() {
        return nom;
    }
    public String getPrenom() {
        return prenom;
    }
    
    @Override
    public String toString(){
        return "\nCLIENT PARTICULIER  "+super.getRefClient()+"\nCivilité : "+civilte+"\nNom : "+nom
                +"\nPrenom : "+prenom;
    }
}
