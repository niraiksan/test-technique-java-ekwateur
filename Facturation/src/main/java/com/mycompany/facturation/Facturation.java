package com.mycompany.facturation;


import static com.mycompany.facturation.TypeEnergie.ELECTRICITE;
import static com.mycompany.facturation.TypeEnergie.GAZ;
import static java.lang.Math.round;
import java.time.YearMonth;
import java.util.*;

/**
 *
 * @author nirai
 */
public class Facturation {
    
    public double facturer(Client client,YearMonth periode){

        double quantiteElectricite = client.getConsommations().stream()
                .filter(consos -> consos.getPeriodeConso().equals(periode))
                .filter(consos -> consos.getTypeEnergie().name().equals("ELECTRICITE"))
                .mapToDouble(consoElectricite -> consoElectricite.getQuantiteKWh())
                .sum();
                
        
        double quantiteGaz = client.getConsommations().stream()
                .filter(consos -> consos.getPeriodeConso().equals(periode))
                .filter(consos -> consos.getTypeEnergie().name().equals("GAZ"))
                .mapToDouble(consoGaz -> consoGaz.getQuantiteKWh())
                .sum();
        
        if(client instanceof ClientParticulier)
            return quantiteElectricite*ELECTRICITE.getMontantParticulier() + quantiteGaz*GAZ.getMontantParticulier();
        
        if(client instanceof ClientPro clientPro)
            return clientPro.estGrandeEntreprise() ? quantiteElectricite*ELECTRICITE.getMontantGrandeEntreprise() 
                    + quantiteGaz*GAZ.getMontantGrandeEntreprise():
            quantiteElectricite*ELECTRICITE.getMontantPetiteEntreprise() + quantiteGaz*GAZ.getMontantPetiteEntreprise();
                 
        
        return -1; // Valeur de retour par défaut si le type de client n'est pas reconnu
    }

    
    
    public static void main(String[] args) {
        System.out.println(" -------------------------------------------------------------------------------------------------");
        System.out.println("| TEST TECHNIQUE JAVA BACKEND - EKWATEUR                                                          |");
        System.out.println("| Niraiksan Ravimohan Master MIAGE à l'Université Paris Nanterre                                  |");
        System.out.println("|                                                                                                 |");
        System.out.println("| J'ai prealablement crée des clients et enregistré leurs consommations d'energie.                |");
        System.out.println(" -------------------------------------------------------------------------------------------------");
        System.out.println();
        
        
            //Création de clients et enregistrement de leurs consommations d'énergie
        ClientParticulier client1   = new ClientParticulier("M.","Ravimohan","Niraiksan","EKW00000001");
        ClientPro clientPro = new ClientPro("12345678900010", "Entreprise ABC",24000,"EKW00000002");
        
        ArrayList<Consommation> listeConsommation = new ArrayList<>(Arrays.asList(
            new Consommation(GAZ, 2, YearMonth.of(2023, 6)),
            new Consommation(ELECTRICITE, 13, YearMonth.of(2023, 6)),
            new Consommation(GAZ, 12, YearMonth.of(2023, 7)),
            new Consommation(ELECTRICITE, 12, YearMonth.of(2023, 7)),
            new Consommation(GAZ, 5, YearMonth.of(2023, 8)),
            new Consommation(ELECTRICITE, 2, YearMonth.of(2023, 9))
        ));
        
        client1.ajouterConsommation(listeConsommation);
        clientPro.ajouterConsommation(listeConsommation);
             
       
            //L'utilisateur choisit la période (mois et année) pour laquelle il souhaite établir la facture.
        int annee,mois;
        YearMonth periode;
                
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Veuillez entrer l'année pour laquelle vous souhaitez obtenir la facture : ");
            while (!scanner.hasNextInt()) {
                System.out.println("Veuillez entrer une valeur numérique pour l'année : ");
                scanner.next(); // Lire la valeur non numérique et la rejeter
            }
            annee = scanner.nextInt();

            System.out.println("Veuillez entrer le mois pour lequel vous souhaitez obtenir la facture (entre 1 et 12) : ");
            while (!scanner.hasNextInt()) {
                System.out.println("Veuillez entrer une valeur numérique correcte pour le mois : ");
                scanner.next();
            }
            mois = scanner.nextInt();
        } 
        
        periode = YearMonth.of(annee,mois);
        
        
            //Calcul du montant de la facture
        double montant_client1  =   round(new Facturation().facturer(client1,periode)*100.0)/100.0;
        double montant_clientPro    =   round(new Facturation().facturer(clientPro,periode)*100.0)/100.0;
        
        
            //Affichage dans la console
        System.out.println("\n\nPour la periode "+periode.toString()+" il y a :");
       
        System.out.println(client1.toString()+"\nLe montant à facturer est de : "+montant_client1);
        System.out.println(clientPro.toString()+"\nLe montant à facturer est de : "+montant_clientPro);
     
    }
    
}
