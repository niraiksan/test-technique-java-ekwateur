/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.facturation;

/**
 *
 * @author nirai
 */
public enum TypeEnergie {
    ELECTRICITE(0.121,0.118,0.114),
    GAZ(0.115,0.113,0.111);

    private final double montantParticulier;
    private final double montantPetiteEntreprise;
    private final double montantGrandeEntreprise;

    private TypeEnergie(double montantParticulier, double montantPetiteEntreprise, double montantGrandeEntreprise) {
        this.montantParticulier = montantParticulier;
        this.montantPetiteEntreprise = montantPetiteEntreprise;
        this.montantGrandeEntreprise = montantGrandeEntreprise;
    }

    public double getMontantParticulier() {
        return montantParticulier;
    }

    public double getMontantPetiteEntreprise() {
        return montantPetiteEntreprise;
    }

    public double getMontantGrandeEntreprise() {
        return montantGrandeEntreprise;
    }
    
}
