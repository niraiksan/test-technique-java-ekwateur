package com.mycompany.facturation;

/**
 *
 * @author nirai
 */
public class ClientPro extends Client{
    private String siret;
    private String raisonSociale;
    private double CA;

    public ClientPro(String siret, String raisonSociale, double CA, String refClient) {
        super(refClient);
        this.siret = siret;
        this.raisonSociale = raisonSociale;
        this.CA = CA;
    }

    public String getSiret() {
        return siret;
    }
    public String getRaisonSociale() {
        return raisonSociale;
    }
    public double getCA() {
        return CA;
    }
    
    @Override
    public String toString(){
        return "\nCLIENT PRO  "+super.getRefClient()+"\nNumero de Siret : "+siret+"\nRaison Sociale : "+raisonSociale
                +"\nChiffre d'affaire : "+CA;
    }
    
    public boolean estGrandeEntreprise(){
        return this.CA>=1000000;
    }
            
}
