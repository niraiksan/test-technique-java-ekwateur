package com.mycompany.facturation;

import java.util.ArrayList;

/**
 *
 * @author nirai
 */
public class Client {
    private String refClient;
    private ArrayList<Consommation> consommations;
  
    public Client(String refClient){
        this.refClient=refClient;
        this.consommations = new ArrayList<>();
    }

    public String getRefClient() {
        return refClient;
    }
    public ArrayList<Consommation> getConsommations() {
        return consommations;
    }
  
    public void ajouterConsommation(Consommation consommation) {
        consommations.add(consommation);
    }
    public void ajouterConsommation(ArrayList<Consommation> listeConso) {
        consommations.addAll(listeConso);
    }
}
