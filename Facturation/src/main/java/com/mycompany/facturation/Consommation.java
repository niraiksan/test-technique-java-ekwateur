package com.mycompany.facturation;

import java.time.YearMonth;

/**
 *
 * @author nirai
 */

public class Consommation {
    
    private TypeEnergie typeEnergie; 
    private double quantiteKWh;         //quantité d'énergie consommée en kWh
    private YearMonth periodeConso;     //période de consommation : mois et année 

    public Consommation(TypeEnergie typeEnergie, double quantite, YearMonth periodeConso) {
        this.typeEnergie = typeEnergie;
        this.quantiteKWh = quantite;
        this.periodeConso = periodeConso;
    }

    public TypeEnergie getTypeEnergie() {
        return typeEnergie;
    }
    public double getQuantiteKWh() {
        return quantiteKWh;
    }
    public YearMonth getPeriodeConso() {
        return periodeConso;
    }
    
}
